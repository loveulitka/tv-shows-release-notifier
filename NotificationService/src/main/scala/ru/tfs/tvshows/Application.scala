package ru.tfs.tvshows

import scala.util.Random.nextInt

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import net.ceedubs.ficus.Ficus._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging


object Application extends App with StrictLogging {

  println("Hello Cats!")

  val config: Config = ConfigFactory.load()
  val jobPeriod = config.as[Int]("jobPeriodInSeconds")

  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val jobe: JobExecutor = new JobExecutor(workerId = nextInt(1000))


  class JobExecutorRunner(implicit val jobExec: JobExecutor)
    extends Actor {

    override def receive: Receive = {
      case DoYourJob =>
        logger.debug(s"start execution, workerId ${jobe.workerId}")
        jobExec.startExecution()
    }
  }


  object JobExecutorRunner {
    def props(): Props = Props(new JobExecutorRunner())
  }


  val scheduler = QuartzSchedulerExtension(actorSystem)
  val jobExecutorActor = actorSystem.actorOf(JobExecutorRunner.props())

  //A message object, which will be sent to `receiver` each time the schedule fires
  case object DoYourJob


  scheduler.createSchedule("PeriodicJob", None, s"*/$jobPeriod * * ? * *")
  scheduler.schedule("PeriodicJob", jobExecutorActor, DoYourJob)

}
