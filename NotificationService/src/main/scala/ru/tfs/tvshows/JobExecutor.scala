package ru.tfs.tvshows

import java.util.UUID

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

import akka.http.scaladsl.model.{HttpResponse, StatusCode}
import net.ceedubs.ficus.Ficus._
import com.typesafe.config.{Config, ConfigFactory}
import com.typesafe.scalalogging.StrictLogging

import ru.tfs.tvshows.httpclient.{SlackSender, VKSender, WebhookSender}
import ru.tfs.tvshows.storage.JobStatusEnum.JobStatus
import ru.tfs.tvshows.storage.{DbClient, EndpointTypeEnum, Job, JobStatusEnum, UserEndpoint}


class JobExecutor (val workerId: Int) extends StrictLogging {

  val dbClient = new DbClient
  val webhookSender = new WebhookSender
  val slackSender = new SlackSender
  val emailSender = new EmailSender
  val vkSender = new VKSender

  val config: Config = ConfigFactory.load()
  val maxAttempts: Int = config.as[Int]("maxAttempts")

  //check for jobs in queue and get one
  def getJobFromQueue(): Future[Option[Job]] = {
    logger.debug("try to get jobs from queue")
    dbClient.getJobsInQueue(worker = workerId, maxAttempts = maxAttempts).recover {
      case e =>
        logger.error(s"Can't get jobs from queue because of ${e.getMessage}")
        throw e
    }
  }

  //get endpoint for the job
  def getEndpointForJob(endpointId: UUID): Future[UserEndpoint] = {
    dbClient.getEndpointById(endpointId).map {
      case Some(endpoint) => endpoint
      case None => throw new NoSuchElementException(s"no valid endpoints for endpoint_id $endpointId")
    }
  }

  //try to send
  def tryToExecuteJob(job: Job, endpoint: UserEndpoint): Future[JobStatusEnum.Value] = {
    val response = endpoint.endpointType match {
      case EndpointTypeEnum.Slack => slackSender.sendSlackNotification(sendBody = job.notificationInfo, uri = endpoint.endpointInfo)
      case EndpointTypeEnum.Email => emailSender.sendEmail(sendBody = job.notificationInfo, address = endpoint.endpointInfo)
      case EndpointTypeEnum.VK => vkSender.sendVKNotification(sendBody = job.notificationInfo, userId = endpoint.endpointInfo)
      case EndpointTypeEnum.Webhook | _ => webhookSender.sendWebhook(sendBody = job.notificationInfo, uri = endpoint.endpointInfo)
    }

    response.transform {
      case Success(resp: HttpResponse) if resp.status == StatusCode.int2StatusCode(200) => Success(JobStatusEnum.Success)
      case Success(resp: HttpResponse) if resp.status != StatusCode.int2StatusCode(200) => Success(JobStatusEnum.InQueue)
      case Success(EmailSentSuccess) => Success(JobStatusEnum.Success)
      case Failure(ex) => Success(JobStatusEnum.InQueue)
    }
  }

  //report the result
  def setJobStatus(jobId: UUID, status: JobStatus, workerIdOpt: Option[Int] = Option(workerId)): Future[Unit] = {
    dbClient.setJobStatus(jobId = jobId, status = status, workerId = workerIdOpt)
  }

  //start execution
  def startExecution(): Future[Any] = {
    getJobFromQueue().flatMap {    //check for enqueued jobs
      case Some(job) => {
        logger.debug(s"i have some job to do: $job")
        getEndpointForJob(job.endpointId).transformWith {    // get endpoint
          case Success(endp) => {
            logger.debug(s"found endpoint for my job $endp")
            tryToExecuteJob(job, endp).map { status =>    // send data
              logger.debug(s"done with status $status")
              setJobStatus(job.jobId, status).transformWith {    // next
                case Success(_) => startExecution()
                case Failure(e) => Future.successful(e.getMessage)
              }
            }
          }
          case Failure(e) =>
            logger.debug(s"some troubles with endpoints =( ${e.getMessage}")
            setJobStatus(job.jobId, JobStatusEnum.InQueue).transformWith {    // next
              case Success(_) => startExecution()
              case Failure(ex) => Future.successful(e.getMessage)
            }
        }
      }
      case _ => Future.successful(logger.debug("All jobs are done!!!"))
    }
  }
}
