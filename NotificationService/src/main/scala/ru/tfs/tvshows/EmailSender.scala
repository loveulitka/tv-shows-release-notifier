package ru.tfs.tvshows

import javax.mail._
import javax.mail.internet._
import java.util._

import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

import net.ceedubs.ficus.Ficus._
import com.typesafe.config.{Config, ConfigFactory}


sealed trait EmailSentStatus

case class EmailSentSuccess() extends EmailSentStatus
case class EmailSentFailure(message: String) extends Throwable with EmailSentStatus


class EmailSender {

  val config: Config = ConfigFactory.load()
  val username: String = config.as[String]("gmailUser")
  val password: String = config.as[String]("gmailPass")
  val name: String = config.as[String]("gmailName")
  val subject: String = config.as[String]("gmailSubject")


  val properties = new Properties()
  properties.put("mail.smtp.host", "smtp.gmail.com")
  properties.put("mail.smtp.starttls.enable","true")

  def sendEmail(sendBody: String, address: String): Future[EmailSentSuccess.type] = {
    Future {
      val session = Session.getDefaultInstance(properties)
      val message = new MimeMessage(session)

      message.setFrom(s"$name <$username>")
      message.addRecipients(Message.RecipientType.TO,
        s"User Name <$address>")
      message.setSubject(s"$subject")
      message.setText(s"$sendBody")

      Transport.send(message, username, password)
    }.transform {
      case Success(_) => Success(EmailSentSuccess)
      case Failure(e) => Failure(EmailSentFailure(e.getMessage))
    }
  }
}
