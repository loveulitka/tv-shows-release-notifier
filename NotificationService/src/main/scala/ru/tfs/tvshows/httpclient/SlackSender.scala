package ru.tfs.tvshows.httpclient

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.Random.nextInt

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse}
import net.ceedubs.ficus.Ficus._
import com.typesafe.config.{Config, ConfigFactory}


class SlackSender {

  val config: Config = ConfigFactory.load()
  val slackBotName: String = config.as[String]("slackBotName")
  val slackBotIcon: List[String] = config.as[List[String]]("slackBotIcon")

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  def sendSlackNotification(sendBody: String, uri: String): Future[HttpResponse] = {
    val getRandomIcon = slackBotIcon(nextInt(slackBotIcon.length))
    val body =
      s"""{
         |"text": "$sendBody",
         |"username": "$slackBotName",
         |"icon_emoji": "$getRandomIcon"
         |}""".stripMargin

    val response: Future[HttpResponse] = Http().singleRequest(HttpRequest(
      method = HttpMethods.POST,
      uri = uri,
      entity = HttpEntity(ContentTypes.`application/json`, body)
    ))
    response
  }
}
