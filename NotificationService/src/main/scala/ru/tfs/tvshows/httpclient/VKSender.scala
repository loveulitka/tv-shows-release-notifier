package ru.tfs.tvshows.httpclient

import scala.concurrent.{ExecutionContextExecutor, Future}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest, HttpResponse}
import net.ceedubs.ficus.Ficus._
import com.typesafe.config.{Config, ConfigFactory}


class VKSender {

  val config: Config = ConfigFactory.load()
  val vkUrl: String = config.as[String]("vkUrl")
  val vkApiKey: String = config.as[String]("vkApiKey")

  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  def sendVKNotification(sendBody: String, userId: String): Future[HttpResponse] = {
    val body = s"""message=$sendBody&user_id=$userId&access_token=$vkApiKey&v=5.0"""

    val response: Future[HttpResponse] = Http().singleRequest(HttpRequest(
      method = HttpMethods.POST,
      uri = s"$vkUrl/method/messages.send",
      entity = HttpEntity(ContentTypes.`application/x-www-form-urlencoded`, body)
    ))
    response
  }
}
