package tvshows

import java.util.UUID

import scala.concurrent.duration._
import scala.concurrent.Await

import org.joda.time.DateTime
import org.scalatest.{BeforeAndAfterEach, Suite}

import ru.tfs.tvshows.storage.MyPostgresProfile.api._
import ru.tfs.tvshows.storage.Schema.{db, jobs, userEndpoints, users}
import ru.tfs.tvshows.storage._


trait TestContext extends BeforeAndAfterEach { this: Suite =>

  val testUser = User(userId = UUID.randomUUID(), firstName = "Nastya", lastName = "Fuckastya")

  val webhookEndp = UserEndpoint(
    endpointId = UUID.randomUUID(),
    userId = testUser.userId,
    endpointType = EndpointTypeEnum.Webhook,
    endpointInfo = "https://webhook.site/a9f422e0-7bd2-4e3a-be24-4af3f5c41ec1",
    flagValid = true
  )

  val invalidEndp = webhookEndp.copy(
    flagValid = false
  )

  val brokenWebhookEndp = webhookEndp.copy(
    endpointInfo = "https://webhook.site/a9f422e0"
  )

  val slackEndp = webhookEndp.copy(
    endpointType = EndpointTypeEnum.Slack,
    endpointInfo = "https://hooks.slack.com/services/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",
  )

  val brokenSlackEndp = slackEndp.copy(
    endpointInfo = "https://hooks.slack.com/services/333333333/2222222222/1111111111111",
  )

  val emailEndp = webhookEndp.copy(
    endpointType = EndpointTypeEnum.Email,
    endpointInfo = "ortofoxnastya@gmail.com",
  )

  val vkEndp =  webhookEndp.copy(
    endpointType = EndpointTypeEnum.VK,
    endpointInfo = "99970727",
  )

  val testJob = Job(
    jobId = UUID.randomUUID(),
    notificationInfo = "Hello Cats!!! Game of Thrones: season 8, episode 2 is released",
    endpointId = webhookEndp.endpointId,
    attempts = 0,
    status = JobStatusEnum.InQueue,
    created_at = DateTime.now,
    updated_at = DateTime.now
  )


  val dbClientTest = new DbClient

  override def beforeEach() {
    Await.result(db.run(jobs.delete), 2 seconds)
    Await.result(db.run(userEndpoints.delete), 2 seconds)
    Await.result(db.run(users.delete), 2 seconds)
    super.beforeEach() // To be stackable, must call super.beforeEach
  }

  override def afterEach() {
    try {
      super.afterEach() // To be stackable, must call super.afterEach
    }
    finally {
//      Await.result(db.run(jobs.delete), 2 seconds)
//      Await.result(db.run(userEndpoints.delete), 2 seconds)
//      Await.result(db.run(users.delete), 2 seconds)
    }
  }
}
