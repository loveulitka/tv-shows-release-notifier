package tvshows

import scala.concurrent.Await
import scala.concurrent.duration._

import org.scalatest.{FreeSpec, Matchers}

import ru.tfs.tvshows.{EmailSender, EmailSentSuccess}


class EmailSenderSpec extends FreeSpec with Matchers {

  val emailSender = new EmailSender

  "EmailSender" - {
    "send email notification to the address with test text" - {
      "response should be success" in {
        Await.result(
          emailSender.sendEmail(sendBody = "Hello Cats!!! Game of Thrones: season 8, episode 2 is released",
            address = "ortofoxnastya@gmail.com"
          ), 3 seconds) shouldBe EmailSentSuccess
      }
    }
  }
}
