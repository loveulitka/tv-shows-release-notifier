package tvshows

import akka.http.scaladsl.model.StatusCode
import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import org.scalatest.{FreeSpec, Matchers}
import ru.tfs.tvshows.httpclient.WebhookSender

import scala.concurrent.Await
import scala.concurrent.duration._


class WebhookSenderSpec extends FreeSpec with Matchers with TestContext {

  val webhookSender = new WebhookSender

  val config: Config = ConfigFactory.load()
  val uri: String = config.as[String]("webhookURI")

  "WebhookSender" - {
    "send webhook to url with text" - {
      "response should be 200" in {
        Await.result(webhookSender.sendWebhook("test message", uri),
          2 seconds).status shouldBe StatusCode.int2StatusCode(200)
      }

      "for invalid uri: response should be error response code" in {
        Await.result(webhookSender.sendWebhook("test message", brokenWebhookEndp.endpointInfo),
          2 seconds).status shouldBe StatusCode.int2StatusCode(404)
      }
    }
  }
}