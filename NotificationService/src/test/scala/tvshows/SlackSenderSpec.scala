package tvshows

import scala.concurrent.Await
import scala.concurrent.duration._

import akka.http.scaladsl.model.StatusCode
import com.typesafe.config.{Config, ConfigFactory}
import net.ceedubs.ficus.Ficus._
import org.scalatest.{FreeSpec, Matchers}

import ru.tfs.tvshows.httpclient.SlackSender


class SlackSenderSpec extends FreeSpec with Matchers with TestContext {

  val slackSender = new SlackSender

  val config: Config = ConfigFactory.load()
  val uri: String = config.as[String]("slackURI")

  "SlackSender" - {
    "send slack notification to url with text" - {
      "response should be 200" in {
        Await.result(slackSender.sendSlackNotification("test message", uri),
          2 seconds).status shouldBe StatusCode.int2StatusCode(200)
      }
      "for invalid uri: response should be error code" in {
        Await.result(slackSender.sendSlackNotification("test message", brokenSlackEndp.endpointInfo),
          2 seconds).status shouldBe StatusCode.int2StatusCode(404)
      }
    }
  }
}
