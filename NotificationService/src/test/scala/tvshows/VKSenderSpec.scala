package tvshows

import akka.http.scaladsl.model.StatusCode
import org.scalatest.{FreeSpec, Matchers}
import ru.tfs.tvshows.httpclient.VKSender

import scala.concurrent.Await
import scala.concurrent.duration._


class VKSenderSpec extends FreeSpec with Matchers with TestContext {

  val vkSender = new VKSender

  "VKSender" - {
    "send vk notification to url with text" - {
      "response should be 200" in {
        Await.result(vkSender.sendVKNotification(sendBody = "test message", userId = "99970727"),
          2 seconds).status shouldBe StatusCode.int2StatusCode(200)
      }
    }
  }
}
