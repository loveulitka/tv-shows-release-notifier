package tvshows

import java.util.UUID

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

import org.joda.time.DateTime
import org.scalatest._

import ru.tfs.tvshows.JobExecutor
import ru.tfs.tvshows.storage.MyPostgresProfile.api._
import ru.tfs.tvshows.storage.Schema.{db, jobs, userEndpoints, users}
import ru.tfs.tvshows.storage.{DbClient, JobStatusEnum}


class JobExecutorSpec extends FreeSpec
  with Matchers
  with TestContext {

  val dbClient = new DbClient
  val jobe = new JobExecutor(13)

 //beforeEach is here!
  "getJobFromQueue" - {
    "get jobs in status InQueue" - {
      "and return Option(None), because there are 0 jobs in queue" in {
        Await.result(jobe.getJobFromQueue(), 2 seconds).isDefined shouldBe false
      }

      "and return job to do" in {
        Await.result(db.run(users += testUser), 2 seconds)
        Await.result(db.run(userEndpoints += webhookEndp), 2 seconds)
        Await.result(db.run(jobs += testJob), 2 seconds)

        val res = Await.result(jobe.getJobFromQueue(), 2 seconds)
        res.isDefined shouldBe true
        res.map { job =>
          job.jobId shouldBe testJob.jobId
          job.notificationInfo shouldBe testJob.notificationInfo
          job.endpointId shouldBe testJob.endpointId
          job.attempts shouldBe testJob.attempts
          job.status shouldBe JobStatusEnum.InProgress
          job.created_at shouldBe testJob.created_at
          job.worker_id shouldBe Some(jobe.workerId)
        }
      }

      "and return the oldest job with count of attempts less then the value in config" in {
        //new jobs
        val newJob = testJob.copy(jobId = UUID.randomUUID(), created_at = DateTime.now)
        val jobManyAttempts = testJob.copy(jobId = UUID.randomUUID(), created_at = DateTime.now, attempts = 10)

        Await.result(db.run(users += testUser), 2 seconds)
        Await.result(db.run(userEndpoints += webhookEndp), 2 seconds)
        Await.result(db.run(jobs += jobManyAttempts), 2 seconds)
        Await.result(db.run(jobs += testJob), 2 seconds)
        Await.result(db.run(jobs += newJob), 2 seconds)

        val res = Await.result(jobe.getJobFromQueue(), 2 seconds)
        res.isDefined shouldBe true
        res.map { job =>
          job.jobId shouldBe testJob.jobId
          job.notificationInfo shouldBe testJob.notificationInfo
          job.endpointId shouldBe testJob.endpointId
          job.attempts shouldBe testJob.attempts
          job.status shouldBe JobStatusEnum.InProgress
          job.created_at shouldBe testJob.created_at
          job.worker_id shouldBe Some(jobe.workerId)
        }
      }
    }
  }

  "getEndpointForJob" - {
    "no such endpoint in db" in {
      intercept[NoSuchElementException] {
        Await.result(jobe.getEndpointForJob(UUID.randomUUID()), 2 seconds)
      }
    }

    "endpoint with flag_valid = false" in {
      Await.result(db.run(users += testUser), 2 seconds)
      Await.result(db.run(userEndpoints += invalidEndp), 2 seconds)
      intercept[NoSuchElementException] {
        Await.result(jobe.getEndpointForJob(invalidEndp.endpointId), 2 seconds)
      }
    }

    "valid endpoint" in {
      Await.result(db.run(users += testUser), 2 seconds)
      Await.result(db.run(userEndpoints += webhookEndp), 2 seconds)
      Await.result(jobe.getEndpointForJob(webhookEndp.endpointId), 2 seconds) shouldBe webhookEndp
    }
  }

  "tryToExecuteJob" - {
    "successfully sent to webhook" in {
      Await.result(jobe.tryToExecuteJob(testJob, webhookEndp), 2 seconds) shouldBe JobStatusEnum.Success
    }

    "failure sent to webhook" in {
      Await.result(jobe.tryToExecuteJob(testJob, brokenWebhookEndp), 2 seconds) shouldBe JobStatusEnum.InQueue
    }

    "successfully sent to slack" in {
      Await.result(jobe.tryToExecuteJob(testJob, slackEndp), 2 seconds) shouldBe JobStatusEnum.Success
    }

    "failure sent to slack" in {
      Await.result(jobe.tryToExecuteJob(testJob, brokenSlackEndp), 2 seconds) shouldBe JobStatusEnum.InQueue
    }

    "successfully sent to email" in {
      Await.result(jobe.tryToExecuteJob(testJob, emailEndp), 4 seconds) shouldBe JobStatusEnum.Success
    }

    "successfully sent to vk" in {
      Await.result(jobe.tryToExecuteJob(testJob, vkEndp), 3 seconds) shouldBe JobStatusEnum.Success
    }
  }

  "setJobResult: status Success + attempts increment" in {
    Await.result(db.run(users += testUser), 2 seconds)
    Await.result(db.run(userEndpoints += webhookEndp), 2 seconds)
    Await.result(db.run(jobs += testJob), 2 seconds)

    Await.result(jobe.setJobStatus(
      jobId = testJob.jobId,
      status = JobStatusEnum.Success,
      workerIdOpt  = Some(jobe.workerId)
    ), 2 seconds)

    val res = Await.result(db.run(jobs.filter(_.jobId === testJob.jobId).result.headOption), 2 seconds)
    res.isDefined shouldBe true
    res.map { job =>
      job.attempts shouldBe testJob.attempts + 1
      job.status shouldBe JobStatusEnum.Success
      job.worker_id shouldBe Some(jobe.workerId)
    }
  }

  "startExecution" - {
    "success job" in {
      //sometimes failed, because endpoint is real
      Await.result(db.run(users += testUser), 2 seconds)
      Await.result(db.run(userEndpoints += webhookEndp), 2 seconds)
      Await.result(db.run(jobs += testJob), 2 seconds)
      Await.result(db.run(jobs += testJob.copy(jobId = UUID.randomUUID(), notificationInfo = "lalalalala")), 2 seconds)

      Await.result(jobe.startExecution(), 2 seconds)
      Thread.sleep(1000) // pg magic: test get status == InProgress, then i run query and get Success
      val res = Await.result(db.run(jobs.filter(_.jobId === testJob.jobId).result.headOption), 2 seconds)
      res.isDefined shouldBe true
      res.map { job =>
        job.status shouldBe JobStatusEnum.Success
      }
    }

    "failed job: invalid endpoint" in {
      Await.result(db.run(users += testUser), 2 seconds)
      Await.result(db.run(userEndpoints += invalidEndp), 2 seconds)
      Await.result(db.run(jobs += testJob), 2 seconds)

      Await.result(jobe.startExecution(), 2 seconds)
      Thread.sleep(1000) // pg magic
      val res = Await.result(db.run(jobs.filter(_.jobId === testJob.jobId).result.headOption), 2 seconds)
      res.isDefined shouldBe true
      res.map { job =>
        job.status shouldBe JobStatusEnum.InQueue
      }
    }

    "failed job: fail to send" in {
      Await.result(db.run(users += testUser), 2 seconds)
      Await.result(db.run(userEndpoints += brokenWebhookEndp), 2 seconds)
      Await.result(db.run(jobs += testJob), 2 seconds)

      Await.result(jobe.startExecution(), 5 seconds)
      Thread.sleep(1500) // pg magic
      val res = Await.result(db.run(jobs.filter(_.jobId === testJob.jobId).result.headOption), 2 seconds)
      res.isDefined shouldBe true
      res.map { job =>
        job.status shouldBe JobStatusEnum.InQueue
      }
    }
  }
}
