# TV-Shows-Release-Notifier
Нотификация пользователей по разным каналам о выходе новых серий сериалов

## How to launch
1. Docker installation
2. `docker login registry.gitlab.com`
3. `sbt notificationService/docker:publishLocal` или `sbt notificationService/docker:publish`
4. `sbt aggregationService/docker:publishLocal` или `sbt aggregationService/docker:publish`
5. `docker build --no-cache -t registry.gitlab.com/loveulitka/tv-shows-release-notifier/mypg:1-SNAPSHOT -f ./Dockerfile .`
6. `docker push registry.gitlab.com/loveulitka/tv-shows-release-notifier/mypg:1-SNAPSHOT`
7. `docker network create --driver=bridge tfs-network`
8. `docker run -d --rm --net=tfs-network --name pg registry.gitlab.com/loveulitka/tv-shows-release-notifier/mypg:1-SNAPSHOT`
9. `docker run -d --rm --net=tfs-network --name notif registry.gitlab.com/loveulitka/tv-shows-release-notifier/notificationservice:0.5-SNAPSHOT`
10. `docker run -d --rm --net=tfs-network --name agg registry.gitlab.com/loveulitka/tv-shows-release-notifier/aggregationservice:0.5-SNAPSHOT`

### Статьи на тему:
1. https://medium.com/jeroen-rosenberg/lightweight-docker-containers-for-scala-apps-11b99cf1a666
2. https://blog.csainty.com/2016/07/connecting-docker-containers.html


## Схема взаимодействия компонентов
![Схема взаимодействия компонентов](./docs/system_schema.jpg "Схема взаимодействия компонентов")


## Схема бд
Создание схемы и таблиц: [V1_create_schema_and_tables.sql](https://gitlab.com/loveulitka/tv-shows-release-notifier/blob/master/src/sql/V1_create_schema_and_tables.sql "Создание схемы и таблиц")

![Схема бд](./docs/db_schema.jpg "Схема бд")


## Фичи проекта
1. Формирование текста нотификаций из данных, полученных из внешнего API
1. Отправка нотификаций пользователям из бд:
   - по вебхукам
   - в slack
   - на email
   - в vk

### Дополнительные фичи
1. Перепосылка в случае неудачной попытки нотификации
2. Взаимодействие с пользователями через VK


## Документация взаимодействия с внешним API (получение новых серий)
Документация: [work-with-external-api.md](https://gitlab.com/loveulitka/tv-shows-release-notifier/blob/master/docs/work-with-external-api.md "Документация")
