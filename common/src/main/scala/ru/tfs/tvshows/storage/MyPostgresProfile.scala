package ru.tfs.tvshows.storage

import com.github.tminglei.slickpg._

trait MyPostgresProfile extends ExPostgresProfile with PgEnumSupport
  with PgArraySupport
  with PgDate2Support
  with PgRangeSupport
  with PgHStoreSupport
  with PgSearchSupport
  with PgNetSupport
  with PgLTreeSupport
  with PgDateSupportJoda {
  def pgjson = "jsonb" // jsonb support is in postgres 9.4.0 onward; for 9.3.x use "json"

  override val api = MyAPI

  trait MyEnumImplicits {

    implicit val jobStatusTypeMapper = createEnumJdbcType("job_status", JobStatusEnum)
    implicit val jobStatusListTypeMapper = createEnumListJdbcType("job_status", JobStatusEnum)
    implicit val jobStatusColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(JobStatusEnum)
    implicit val jobStatusOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(JobStatusEnum)

    implicit val endpointTypeTypeMapper = createEnumJdbcType("job_status", EndpointTypeEnum)
    implicit val endpointTypeListTypeMapper = createEnumListJdbcType("job_status", EndpointTypeEnum)
    implicit val endpointTypeColumnExtensionMethodsBuilder = createEnumColumnExtensionMethodsBuilder(EndpointTypeEnum)
    implicit val endpointTypeOptionColumnExtensionMethodsBuilder = createEnumOptionColumnExtensionMethodsBuilder(EndpointTypeEnum)

  }

  object MyAPI extends API with ArrayImplicits
    with DateTimeImplicits
    with NetImplicits
    with LTreeImplicits
    with RangeImplicits
    with HStoreImplicits
    with SearchImplicits
    with MyEnumImplicits
    with SearchAssistants {
      implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)
    }
}

object MyPostgresProfile extends MyPostgresProfile
