package ru.tfs.tvshows.storage

import java.util.UUID

import org.joda.time._
import ru.tfs.tvshows.storage.EndpointTypeEnum.EndpointType
import ru.tfs.tvshows.storage.JobStatusEnum.JobStatus
import ru.tfs.tvshows.storage.MyPostgresProfile.api._
import slick.lifted.ProvenShape


object Schema {


  val db = Database.forConfig("db")

  //for test
  class UserInfo(tag: Tag) extends Table[User](tag, _schemaName = Some("tvshows"),_tableName = "user_info") {
    def userId: Rep[UUID] = column("user_id", O.PrimaryKey)
    def firstName: Rep[String] = column("first_name")
    def lastName: Rep[String] = column("last_name")

    override def * : ProvenShape[User] =
      (userId, firstName, lastName) <> (User.tupled, User.unapply)
  }
  val users = TableQuery[UserInfo]


  class Jobs(tag: Tag) extends Table[Job](tag, _schemaName = Some("tvshows"),_tableName = "jobs") {

    def jobId: Rep[UUID] = column("job_id", O.PrimaryKey)
    def notificationInfo: Rep[String] = column("notification_info")
    def endpointId: Rep[UUID] = column("endpoint_id")
    def attempts: Rep[Int] = column("attempts")
    def status: Rep[JobStatus] = column("status")
    def created_at: Rep[DateTime] = column("created_at")
    def updated_at: Rep[DateTime] = column("updated_at")
    def worker_id: Rep[Option[Int]] = column("worker_id")

    override def * : ProvenShape[Job] =
      (jobId, notificationInfo, endpointId, attempts, status, created_at, updated_at, worker_id) <> (Job.tupled, Job.unapply)
  }
  val jobs = TableQuery[Jobs]


  class UserEndpoints(tag: Tag) extends Table[UserEndpoint](tag, _schemaName = Some("tvshows"), _tableName = "user_endpoints") {

    def endpointId: Rep[UUID] = column("endpoint_id", O.PrimaryKey)
    def userId: Rep[UUID] = column("user_id")
    def endpointType: Rep[EndpointType] = column("endpoint_type")
    def endpointInfo: Rep[String] = column("endpoint_info")
    def flagValid: Rep[Boolean] = column("flag_valid")

    override def * : ProvenShape[UserEndpoint] =
      (endpointId, userId, endpointType, endpointInfo, flagValid) <> (UserEndpoint.tupled, UserEndpoint.unapply)
  }
  val userEndpoints = TableQuery[UserEndpoints]


  class Serials(tag: Tag) extends Table[SerialDB](tag, _schemaName = Some("tvshows"), _tableName = "serials") {
    def serialId: Rep[UUID] = column("serial_id", O.PrimaryKey)
    def externalSerialId: Rep[String] = column("external_serial_id")
    def description: Rep[String] = column("description")
    def lastUpdate: Rep[Long] = column("last_update")

    override def * : ProvenShape[SerialDB] =
      (serialId, externalSerialId, description, lastUpdate) <> (SerialDB.tupled, SerialDB.unapply)
  }

  val serials = TableQuery[Serials]


  class SerialsUsers(tag: Tag) extends Table[SerialUserDB](tag, _schemaName = Some("tvshows"), _tableName = "serials_users") {
    def serialUserId: Rep[UUID] = column("id", O.PrimaryKey)
    def serialId: Rep[UUID] = column("serial_id"/*, ForeignKeyAction.NoAction()*/)
    def userId: Rep[UUID] = column("user_id"/*, ForeignKeyAction.NoAction()*/)
    def flagValid: Rep[Boolean] = column("flag_valid")
    def lastModifiedEpisode: Rep[String] = column("flag_valid")

    override def * : ProvenShape[SerialUserDB] =
      (serialUserId, serialId, userId, flagValid, lastModifiedEpisode) <> (SerialUserDB.tupled, SerialUserDB.unapply)
  }

  val serialsUsers = TableQuery[SerialsUsers]
}
