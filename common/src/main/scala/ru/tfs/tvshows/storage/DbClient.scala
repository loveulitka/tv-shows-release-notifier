package ru.tfs.tvshows.storage

import java.util.UUID

import com.typesafe.scalalogging.StrictLogging
import org.joda.time.DateTime
import ru.tfs.tvshows.storage.MyPostgresProfile.api._
import ru.tfs.tvshows.storage.Schema._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import org.joda.time.DateTime
import ru.tfs.tvshows.storage.MyPostgresProfile.api._
import ru.tfs.tvshows.storage.Schema._


class DbClient extends StrictLogging {

  def getJobsInQueue(worker: Int, maxAttempts: Int): Future[Option[Job]] = {
    //SELECT job_id FROM jobs WHERE status = 'InQueue' ORDER BY created_at LIMIT 1
    //FOR UPDATE -- SKIP LOCKED
    val selectJob = jobs.filter(j => j.status === JobStatusEnum.InQueue && j.attempts < maxAttempts)
      .sortBy(_.updated_at)
      .take(1)
      .forUpdate
      .result

    //UPDATE jobs SET status = 'InProgress', workerId = ? where job_id = ?
    def updateJob(id: UUID) = {
      jobs.filter(_.jobId === id)
        .map(job => (job.status, job.worker_id, job.updated_at))
        .update((JobStatusEnum.InProgress, Some(worker), DateTime.now))
    }

    val pickAndMarkJob = (for {
      pickJob <- selectJob
      markjob <- DBIO.seq(pickJob.map {job => updateJob(job.jobId)}: _*)
    } yield ()).transactionally

    for {
      _ <- db.run(pickAndMarkJob)
      job <- db.run(jobs.filter(jobToDo => jobToDo.status === JobStatusEnum.InProgress && jobToDo.worker_id === worker)
                        .result
                        .headOption
                    )
    } yield job
  }

  def getEndpointById(endpointId: UUID): Future[Option[UserEndpoint]] =
    db.run(userEndpoints.filter(_.endpointId === endpointId).filter(_.flagValid === true).result.headOption)

  def setJobStatus(jobId: UUID, status: JobStatusEnum.JobStatus, workerId: Option[Int]): Future[Unit] = {
    val action = for {
      attempts <- jobs.filter(_.jobId === jobId).result
      updJob   <- DBIO.seq(attempts.map { att =>
        jobs.filter(_.jobId === jobId)
                     .map(job => (job.attempts, job.status, job.worker_id, job.updated_at))
                     .update((att.attempts + 1, status, workerId, DateTime.now))
      }: _*)
    } yield ()
    db.run(action)
  }

  case class Result(endpId: UUID, firstName: String, lastName: String, serialName: String, serialId: UUID)

  //TODO: add series season and episode to information string
  def addNewJobs(seriesList: Seq[(SerialDB, SeriesUpdate)]): Future[Unit] = {
    def joinTables(serialId: UUID, lastUpdate: Long): Future[Seq[Result]] = {
      val innerJoin = for {
        s <- serials
        u <- serialsUsers if s.serialId === u.serialId
        endp <- userEndpoints if u.userId === endp.userId
        ui <- users if endp.userId === ui.userId
      } yield (endp.endpointId, ui.firstName, ui.lastName, s.description, s.serialId, s.lastUpdate)

      db.run(innerJoin.filter(x => x._5 === serialId.bind && x._6 < lastUpdate.bind).result)
        .map(res => res.map(x => Result(x._1, x._2, x._3, x._4, x._5)))
    }
    val now = new DateTime(System.currentTimeMillis())

    for {
      (serial, serialUpdate) <- seriesList
      jobsData <- joinTables(serial.serialId, serialUpdate.lastUpdated)
      jobData <- jobsData
    } {
      val newJob = Job(UUID.randomUUID(),
        s"Hello, ${jobData.firstName} ${jobData.lastName}!\n ${jobData.serialName}: new episode is released",
        jobData.endpId,
        0,
        JobStatusEnum.InQueue,
        now,
        now
      )
      logger.debug(s"New job: $newJob")
      db.run(jobs += newJob)
      db.run(serials.filter(x => x.serialId === serial.serialId).map(_.lastUpdate).update(serialUpdate.lastUpdated))
    }
    Future.successful(())
  }

  /*//TODO: Maybe this three functions should be run transactionally? (addNewUser -> addNewSerial -> addNewSerialUser)

  def addNewSerialUser(serial: SerialDB, user: User): Future[Unit] = { //TODO: Add new elements when new users is singed up
    db.run(serialsUsers += SerialUserDB(UUID.randomUUID(), user.userId, serial.serialId, flagValid = true, ""))
      .map(_ => ())
  }

  def addNewSeriesList(series: List[Series]): Future[Unit] ={
    val query = series.map(newSeries =>
      serials.filter(serial => serial.externalSerialId === newSeries.id.toString).exists.result.flatMap{ exists =>
        if(!exists)
          serials += SerialDB(UUID.randomUUID(), newSeries.id.toString, newSeries.seriesName)
        else
          DBIO.successful(None)
      }
    )
    val tranQuery = DBIO.sequence(query).transactionally
    db.run(tranQuery).map(_ => ())
  }

  def addNewUser(user: User): Future[Unit] = { //TODO: Add new user (unique)
    db.run(users += user).map(_ => ())
  }*/

  def getAllSeries(): Future[Seq[SerialDB]] = {
    db.run(serials.result)
  }

  def seriesToSerials(seriesList: List[SeriesUpdate]): Future[Seq[(SerialDB, SeriesUpdate)]] = {
    getAllSeries().map{ serialsDb =>
      /*serialsDb.filter{ serial =>
        seriesList.exists(series => serial.externalSerialId == series.id.toString)
      }*/
      val res = for {
        seriesUpdate <- seriesList
        serial <- serialsDb if serial.externalSerialId == seriesUpdate.id.toString
      } yield {
        (serial, seriesUpdate)
      }
      res
    }
  }
}
