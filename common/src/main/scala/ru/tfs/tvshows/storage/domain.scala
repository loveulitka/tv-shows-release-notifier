package ru.tfs.tvshows.storage

import java.util.UUID

import org.joda.time._
import ru.tfs.tvshows.storage.EndpointTypeEnum.EndpointType
import ru.tfs.tvshows.storage.JobStatusEnum.JobStatus

object JobStatusEnum extends Enumeration {
  type JobStatus = Value
  val InQueue, InProgress, Success = Value
}

object EndpointTypeEnum extends Enumeration {
  type EndpointType = Value
  val Webhook, Slack, VK, Email = Value
}

case class Job(
  jobId: UUID,
  notificationInfo: String,
  endpointId: UUID,
  attempts: Int,
  status: JobStatus,
  created_at: DateTime,
  updated_at: DateTime,
  worker_id: Option[Int] = None
)

case class UserEndpoint(endpointId: UUID, userId: UUID, endpointType: EndpointType, endpointInfo: String, flagValid: Boolean)

case class User(userId: UUID, firstName: String, lastName: String)

case class UserLogin(apikey: String, username: String, userkey: String)
case class Jwt(token: String)

case class Series(id: Int, aliases: List[String], banner: String, firstAired: String, network: String,
                  overview: String, seriesName: String, slug: String, status: String)

case class SeriesInfo(id: Int, seriesId: String, seriesName: String)
case class SeriesUpdate(id: Int, lastUpdated: Long)

case class ResponseList(data: List[Series])
case class ResponseInfo(data: SeriesInfo)
case class ResponseUpdate(data: List[SeriesUpdate])

case class SerialUserDB(serialUserId: UUID, serialId: UUID, userId: UUID, flagValid: Boolean, lastModifiedEpisode: String)

case class SerialDB(serialId: UUID, externalSerialId: String, description: String, lastUpdate: Long)