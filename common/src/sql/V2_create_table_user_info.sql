CREATE TABLE tvshows.user_info (
  user_id          uuid NOT NULL UNIQUE,
  first_name       text NOT NULL,
  last_name        text NOT NULL,
  PRIMARY KEY (user_id));

COMMENT ON TABLE tvshows.user_info IS 'Info about users';
COMMENT ON COLUMN tvshows.user_info.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.user_info.first_name IS 'User first name';
COMMENT ON COLUMN tvshows.user_info.last_name IS 'User last name';