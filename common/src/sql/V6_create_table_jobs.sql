CREATE TYPE job_status AS ENUM ('InQueue', 'InProgress', 'Success');

CREATE TABLE tvshows.jobs (
  job_id                   uuid NOT NULL UNIQUE,
  notification_info        text NOT NULL,
  endpoint_id              uuid NOT NULL,
  attempts                 int NOT NULL,
  status                   job_status NOT NULL,
  created_at               timestamp with time zone NOT NULL DEFAULT now(),
  updated_at               timestamp with time zone NOT NULL DEFAULT now(),
  worker_id                int,
  PRIMARY KEY (job_id));
--  CONSTRAINT fk_user_endpoints_endpoint_id FOREIGN KEY (endpoint_id) REFERENCES tvshows.user_endpoints (endpoint_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
--  CONSTRAINT fk_job_status_types_status_type_id FOREIGN KEY (status) REFERENCES tvshows.job_status_types (status_type_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION


COMMENT ON TABLE tvshows.jobs IS 'Queue for notification';
COMMENT ON COLUMN tvshows.jobs.job_id IS 'Unique global job id in our system';
COMMENT ON COLUMN tvshows.jobs.notification_info IS 'Text for notification';
COMMENT ON COLUMN tvshows.jobs.endpoint_id IS 'Unique global endpoint id in our system';
COMMENT ON COLUMN tvshows.jobs.attempts IS 'Count of attempts to send notification';
COMMENT ON COLUMN tvshows.jobs.status IS 'Job status';
COMMENT ON COLUMN tvshows.jobs.created_at IS 'Job creation time';
COMMENT ON COLUMN tvshows.jobs.updated_at IS 'Job last update time';
COMMENT ON COLUMN tvshows.jobs.worker_id IS 'Executor id';