CREATE TABLE tvshows.serials_users (
  id                       uuid NOT NULL UNIQUE,
  serial_id                uuid NOT NULL,
  user_id                  uuid NOT NULL,
  flag_valid               boolean NOT NULL DEFAULT true,
  last_notified_episode    text,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_info_user_id FOREIGN KEY (user_id) REFERENCES tvshows.user_info (user_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_serials_serial_id FOREIGN KEY (user_id) REFERENCES tvshows.serials (serial_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION);

COMMENT ON TABLE tvshows.serials_users IS 'Connections between users and serials';
COMMENT ON COLUMN tvshows.serials_users.serial_id IS 'Unique global serial id in our system';
COMMENT ON COLUMN tvshows.serials_users.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.serials_users.flag_valid IS 'Connection status: activated or deactivated';
COMMENT ON COLUMN tvshows.serials_users.last_notified_episode IS 'Last sent notification';