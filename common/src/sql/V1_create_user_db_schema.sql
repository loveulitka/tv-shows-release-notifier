CREATE USER notifier WITH encrypted password 'notify5000';

CREATE DATABASE tvshows WITH OWNER notifier;

GRANT ALL ON database tvshows TO notifier;

CREATE SCHEMA tvshows;

GRANT ALL ON SCHEMA tvshows TO notifier;

GRANT ALL ON ALL tables IN SCHEMA tvshows TO notifier;