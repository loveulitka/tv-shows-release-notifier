CREATE TABLE tvshows.serials (
  serial_id             uuid NOT NULL UNIQUE,
  external_serial_id    text NOT NULL,
  description           text NOT NULL,
  PRIMARY KEY (serial_id));

COMMENT ON TABLE tvshows.serials IS 'Info about serials';
COMMENT ON COLUMN tvshows.serials.serial_id IS 'Unique global serial id in our system';
COMMENT ON COLUMN tvshows.serials.external_serial_id IS 'Unique global serials id in other systems';
COMMENT ON COLUMN tvshows.serials.description IS 'Serial title';