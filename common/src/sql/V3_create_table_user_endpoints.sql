CREATE TYPE endpoint_type AS ENUM ('Webhook', 'Slack', 'VK', 'Email');

CREATE TABLE tvshows.user_endpoints (
  endpoint_id      uuid NOT NULL UNIQUE,
  user_id          uuid NOT NULL,
  endpoint_type    endpoint_type NOT NULL,
  endpoint_info    text NOT NULL,
  flag_valid       boolean NOT NULL DEFAULT true,
  PRIMARY KEY (endpoint_id),
  CONSTRAINT fk_user_info_user_id FOREIGN KEY (user_id) REFERENCES tvshows.user_info (user_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION);
--  CONSTRAINT fk_endpoints_type_endpoint_type_id FOREIGN KEY (endpoint_type) REFERENCES tvshows.endpoints_type (endpoint_type_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION);

COMMENT ON TABLE tvshows.user_endpoints IS 'Channels for sending notifications';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_id IS 'Unique global endpoint id in our system';
COMMENT ON COLUMN tvshows.user_endpoints.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_type IS 'Endpoint type';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_info IS 'Ids in social networks, emails, webhooks url, etc';
COMMENT ON COLUMN tvshows.user_endpoints.flag_valid IS 'Endpoint status: activated or deactivated';