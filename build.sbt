name := "tv-shows-release-notifier"

scalaVersion := "2.12.8"

lazy val common = project.in(file("common"))
    .settings(
      name := "common",
      version := "0.1-SNAPSHOT",
      libraryDependencies ++= Seq(
        // database
        "com.typesafe.slick"         %% "slick"                    % "3.3.0",
        "com.typesafe.slick"         %% "slick-hikaricp"           % "3.3.0",
        "org.postgresql"             %  "postgresql"               % "42.2.5",
        "com.github.tminglei"        %% "slick-pg"                 % "0.17.2",
        "com.github.tminglei"        %% "slick-pg_joda-time"       % "0.17.2",

        // tests
        "org.scalatest"              %% "scalatest"                % "3.0.5" % Test,

        // other
        "com.typesafe.akka"          %% "akka-http"                % "10.1.8",
        "com.typesafe.akka"          %% "akka-stream"              % "2.5.21",
        "com.iheart"                 %% "ficus"                    % "1.4.5",
        "joda-time"                  %  "joda-time"                % "2.9.9",
        "com.enragedginger"          %% "akka-quartz-scheduler"    % "1.8.0-akka-2.5.x",
        "com.typesafe.scala-logging" %% "scala-logging"            % "3.9.2",
        "ch.qos.logback"             %  "logback-classic"          % "1.2.3",
      )
    )


lazy val aggregationService = project.in(file("AggregationService"))
  .enablePlugins(JavaAppPackaging, AshScriptPlugin)
  .settings(
    mainClass in Compile := Some("ru.tfs.tvshows.Runner.Main"),
    dockerBaseImage := "openjdk:8-jre-alpine",
    dockerRepository := Some("registry.gitlab.com/loveulitka/tv-shows-release-notifier"),
    dockerAlias := DockerAlias(
      registryHost = Some("registry.gitlab.com"),
      username = Some("loveulitka/tv-shows-release-notifier"),
      name = name.value.toLowerCase,
      tag = Some(version.value)
    ),
    name := "aggregationService",
    version := "0.5-SNAPSHOT",
    libraryDependencies ++= Seq(
      "org.json4s"                   %% "json4s-jackson"           % "3.6.5",
      "ch.qos.logback"               % "logback-classic"           % "1.2.3",
      "com.typesafe.scala-logging"   %% "scala-logging"            % "3.9.2",
      "com.softwaremill.sttp"        %% "core"                     % "1.5.11",
      "com.typesafe.akka"            %% "akka-actor"               % "2.5.21",
      "com.typesafe.akka"            %% "akka-stream-testkit"      % "2.5.21" % Test,
      "com.typesafe.akka"            %% "akka-http-testkit"        % "10.1.7" % Test,
      "com.typesafe.akka"            %% "akka-testkit"             % "2.5.21" % Test
    )
  )
  .dependsOn(common % "compile->compile;test->test")

lazy val notificationService = project.in(file("NotificationService"))
  .enablePlugins(JavaAppPackaging, AshScriptPlugin)
  .settings(
    mainClass in Compile := Some("ru.tfs.tvshows.Application"),
    dockerBaseImage := "openjdk:8-jre-alpine",
    dockerRepository := Some("registry.gitlab.com/loveulitka/tv-shows-release-notifier"),
    dockerAlias := DockerAlias(
      registryHost = Some("registry.gitlab.com"),
      username = Some("loveulitka/tv-shows-release-notifier"),
      name = name.value.toLowerCase,
      tag = Some(version.value)
    ),
    name := "notificationService",
    version := "0.5-SNAPSHOT",
    libraryDependencies ++= Seq(
      "javax.mail"                   % "mail"                      % "1.5.0-b01"
    )
  )
  .dependsOn(common % "compile->compile;test->test")
