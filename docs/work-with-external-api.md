# Полная документация
https://api.thetvdb.com/swagger

# Используемые методы
Получение JWT key
```
Request
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d {"apikey":"APIKEY","username":"USERNAME","userkey":"USERKEY"} 'https://api.thetvdb.com/login'

Response
{"token": "string"}
```
 
Поиск сериала по названию
```
Request
curl -X GET --header 'Accept: application/json' --header 'Accept-Language: en' --header 'Authorization: Bearer <JWT key>' 'https://api.thetvdb.com/search/series?name=<series name>'

Response
{"data":[{"aliases":[],"banner":"graphical/79126-g16.jpg","firstAired":"2002-06-02","id":79126,"network":"HBO","overview":"Unlike most television crime dramas, which neatly introduce and then solve a case all in the space of one hour, HBO's THE WIRE follows one single drug and homicide investigation throughout the length of an entire season. Centered on the drug culture of inner-city Baltimore, the series' storyline unfolds from the points of view of both the criminals lording the streets and the police officers determined to bring them down.","seriesName":"The Wire","slug":"the-wire","status":"Ended"}
```

Получение информации о сериале по id
```
Request
curl -X GET --header 'Accept: application/json' --header 'Accept-Language: en' --header 'Authorization: Bearer <JWT key>' 'https://api.thetvdb.com/series/<id>'

Response
{"data":{"id":360687,"seriesName":"The Wrong Block ","aliases":[],"banner":"","seriesId":"","status":"Ended","firstAired":"","network":"YouTube","networkId":"","runtime":"5","genre":["Action","Animation","Crime"],"overview":"Detective Max Braddock meets with a trusted informant who has information about a plot to kidnap wealthy socialite, Jerrica Ming.","lastUpdated":1552041193,"airsDayOfWeek":"","airsTime":"","rating":"TV-MA","imdbId":"tt5146950","zap2itId":"","added":"2019-03-07 19:47:25","addedBy":522300,"siteRating":0,"siteRatingCount":0,"slug":"the-wrong-block"}}
```

Получение списка сериалов со времени (timestamp)
```
Request
curl -X GET --header 'Accept: application/json' --header 'Authorization: Bearer <JWT key>' 'https://api.thetvdb.com/updated/query?fromTime=<timestamp>'

Response
{"data":[{"id":360348,"lastUpdated":1551437408},{"id":312200,"lastUpdated":1551440108}]}

```