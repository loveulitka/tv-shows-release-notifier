package ru.tfs.tvshows.Logic

import ru.tfs.tvshows.storage._

import scala.concurrent.Future
class StorageLogic {
  private val dbClient = new DbClient

  def addNewJobs(series: Seq[(SerialDB, SeriesUpdate)]): Unit = {
    dbClient.addNewJobs(series)
  }

  def getSerialsDB(seriesList: List[SeriesUpdate]): Future[Seq[(SerialDB, SeriesUpdate)]] = {
    dbClient.seriesToSerials(seriesList)
  }
}
