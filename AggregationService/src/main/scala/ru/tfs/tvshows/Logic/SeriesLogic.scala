package ru.tfs.tvshows.Logic

import ru.tfs.tvshows.Runner.Json4sSupport._
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers.OAuth2BearerToken
import akka.http.scaladsl.model.{HttpMethods, HttpRequest}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import akka.http.scaladsl.model._
import ru.tfs.tvshows.storage._

import scala.concurrent.duration.Duration

/*case class Series(id: Int, aliases: List[String], banner: String, firstAired: String, network: String,
                  overview: String, seriesName: String, slug: String, status: String)

/*case class SeriesInfo(id: Int, seriesId: String, seriesName: String, aliases: List[String], banner: String,
                      status: String, firstAired: String, network: String, networkId: String, runtime: String,
                      genre: List[String], overview: String, lastUpdated: Long, airsDayOfWeek: String, airsTime: String,
                      rating: String, imdbId: String, zap2itId: String, added: String, addedBy: String, siteRating: Int,
                      siteRatingCount: Int, slug: String)*/
case class SeriesInfo(id: Int, seriesId: String, seriesName: String)
case class SeriesUpdate(id: Int, lastUpdated: Long)*/

/*case class ResponseList(data: List[Series])
case class ResponseInfo(data: SeriesInfo)
case class ResponseUpdate(data: List[SeriesUpdate])*/
class SeriesLogic(val loginLogic: LoginLogic)(implicit actorSystem: ActorSystem, actorMaterializer: ActorMaterializer) {
  val jwt: Jwt = Await.result(loginLogic.getToken(), Duration.Inf)

  def getSeriesListByName(name: String, jwt: Jwt = jwt, lang: String = "en"): Future[List[Series]] = {
    val languages = headers.`Accept-Language`(headers.Language(lang))
    val authorization = headers.Authorization(OAuth2BearerToken(jwt.token))
    Http().singleRequest(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"https://api.thetvdb.com/search/series?name=${name.replaceAll(" ", "%20")}"),
      headers = List(authorization, languages)
    )).flatMap(req => Unmarshal(req).to[ResponseList].map(x => x.data))
  }

  def getSeriesInfoById(id: Int, jwt: Jwt = jwt, lang: String = "en"): Future[SeriesInfo] = {
    val languages = headers.`Accept-Language`(headers.Language(lang))
    val authorization = headers.Authorization(OAuth2BearerToken(jwt.token))
    Http().singleRequest(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"https://api.thetvdb.com/series/$id"),
      headers = List(authorization, languages)
    )).flatMap(req => Unmarshal(req).to[ResponseInfo].map(x => x.data))
  }

  def getLastSeriesWithinTimestamp(timestamp: Long, jwt: Jwt = jwt, lang: String = "en"): Future[List[SeriesUpdate]] = {
    val languages = headers.`Accept-Language`(headers.Language(lang))
    val authorization = headers.Authorization(OAuth2BearerToken(jwt.token))
    Http().singleRequest(HttpRequest(
      method = HttpMethods.GET,
      uri = Uri(s"https://api.thetvdb.com/updated/query?fromTime=$timestamp"),
      headers = List(authorization, languages)
    )).flatMap(req => Unmarshal(req).to[ResponseUpdate].map(x => x.data))
  }
}
