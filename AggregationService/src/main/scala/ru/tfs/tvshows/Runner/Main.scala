package ru.tfs.tvshows.Runner

import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.scalalogging.StrictLogging
import org.joda.time.DateTime
import ru.tfs.tvshows.Logic.{LoginLogic, SeriesLogic, StorageLogic}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object Main extends StrictLogging {
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()
  implicit val loginLogic: LoginLogic = new LoginLogic()
  implicit val seriesLogic: SeriesLogic = new SeriesLogic(loginLogic)
  implicit val storageLogic: StorageLogic = new StorageLogic()

  val scheduler: QuartzSchedulerExtension = QuartzSchedulerExtension(actorSystem)

  case object Tick

  class Runner(implicit val seriesLogic: SeriesLogic, storageLogic: StorageLogic) extends Actor {
    override def receive: Receive = {
      case Tick =>
        //val timestamp = (System.currentTimeMillis() - 5.seconds.toMillis) / 1000
        val timestamp = 1556064000
        logger.debug(s"aggregator runs with timestamp: $timestamp (${new DateTime(timestamp.seconds.toMillis)})")
        for {
          seriesUpdateList <- seriesLogic.getLastSeriesWithinTimestamp(timestamp)
          filteredSeries <- {
            logger.debug(seriesUpdateList.take(20).mkString("; "))
            storageLogic.getSerialsDB(seriesUpdateList.take(20))
          }
        } {
          logger.debug(s"Filtered series: $filteredSeries")
          storageLogic.addNewJobs(filteredSeries)
        }
    }
  }

  object Runner { def props(): Props = Props(new Runner())}

  def main(args: Array[String]): Unit = {
    /*val timestamp = 1556064000
    val s = Await.result(seriesLogic.getLastSeriesWithinTimestamp(timestamp), Duration.Inf)
    val s2f = Future.sequence(s.take(10).map(upd => seriesLogic.getSeriesInfoById(upd.id)))
    Await.result(s2f, Duration.Inf).foreach(println)*/
    val secondCount = 10
    val runner = actorSystem.actorOf(Runner.props())
    val scheduleName = s"Every${secondCount}Seconds"
    val scheduleParams = s"*/$secondCount * * ? * *"
    scheduler.createSchedule(scheduleName, None, scheduleParams)
    scheduler.schedule(scheduleName, runner, Tick)
  }
}