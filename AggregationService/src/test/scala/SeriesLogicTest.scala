import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import org.scalatest.{FlatSpecLike, Matchers}
import ru.tfs.tvshows.Logic.{LoginLogic, SeriesLogic}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class SeriesLogicTest extends FlatSpecLike with Matchers {
  implicit val actorSystem: ActorSystem = ActorSystem()
  implicit val actorMaterializer: ActorMaterializer = ActorMaterializer()
  val loginLogic = new LoginLogic()
  val seriesLogic = new SeriesLogic(loginLogic)

  behavior of "getSeriesListByName"
  it should "return sequence of series by name" in {
    val list = Await.result(seriesLogic.getSeriesListByName("The walking dead"), Duration.Inf)
    list.size shouldBe 4
    list.head.id shouldBe 153021
  }

  behavior of "getSeriesInfoById"
  it should "return series info with particular id" in {
    val id = 153021
    val info = Await.result(seriesLogic.getSeriesInfoById(id), Duration.Inf)
    info.seriesName shouldBe "The Walking Dead"
  }
}
