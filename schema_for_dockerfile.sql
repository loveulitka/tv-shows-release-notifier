CREATE USER notifier WITH encrypted password 'notify5000';

CREATE DATABASE tvshows WITH OWNER notifier;

GRANT ALL ON database tvshows TO notifier;
\connect tvshows;
CREATE SCHEMA tvshows;



CREATE TABLE tvshows.user_info (
  user_id          uuid NOT NULL UNIQUE,
  first_name       text NOT NULL,
  last_name        text NOT NULL,
  PRIMARY KEY (user_id));

COMMENT ON TABLE tvshows.user_info IS 'Info about users';
COMMENT ON COLUMN tvshows.user_info.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.user_info.first_name IS 'User first name';
COMMENT ON COLUMN tvshows.user_info.last_name IS 'User last name';



CREATE TYPE endpoint_type AS ENUM ('Webhook', 'Slack', 'VK', 'Email');

CREATE TABLE tvshows.user_endpoints (
  endpoint_id      uuid NOT NULL UNIQUE,
  user_id          uuid NOT NULL,
  endpoint_type    endpoint_type NOT NULL,
  endpoint_info    text NOT NULL,
  flag_valid       boolean NOT NULL DEFAULT true,
  PRIMARY KEY (endpoint_id),
  CONSTRAINT fk_user_info_user_id FOREIGN KEY (user_id) REFERENCES tvshows.user_info (user_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION);

COMMENT ON TABLE tvshows.user_endpoints IS 'Channels for sending notifications';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_id IS 'Unique global endpoint id in our system';
COMMENT ON COLUMN tvshows.user_endpoints.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_type IS 'Endpoint type';
COMMENT ON COLUMN tvshows.user_endpoints.endpoint_info IS 'Ids in social networks, emails, webhooks url, etc';
COMMENT ON COLUMN tvshows.user_endpoints.flag_valid IS 'Endpoint status: activated or deactivated';



CREATE TABLE tvshows.serials (
  serial_id             uuid NOT NULL UNIQUE,
  external_serial_id    text NOT NULL,
  description           text NOT NULL,
  last_update           int NOT NULL,
  PRIMARY KEY (serial_id));

COMMENT ON TABLE tvshows.serials IS 'Info about serials';
COMMENT ON COLUMN tvshows.serials.serial_id IS 'Unique global serial id in our system';
COMMENT ON COLUMN tvshows.serials.external_serial_id IS 'Unique global serials id in other systems';
COMMENT ON COLUMN tvshows.serials.description IS 'Serial title';



CREATE TABLE tvshows.serials_users (
  id                       uuid NOT NULL UNIQUE,
  serial_id                uuid NOT NULL,
  user_id                  uuid NOT NULL,
  flag_valid               boolean NOT NULL DEFAULT true,
  last_notified_episode    text,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_info_user_id FOREIGN KEY (user_id) REFERENCES tvshows.user_info (user_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_serials_serial_id FOREIGN KEY (serial_id) REFERENCES tvshows.serials (serial_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION);

COMMENT ON TABLE tvshows.serials_users IS 'Connections between users and serials';
COMMENT ON COLUMN tvshows.serials_users.serial_id IS 'Unique global serial id in our system';
COMMENT ON COLUMN tvshows.serials_users.user_id IS 'Unique global user id in our system';
COMMENT ON COLUMN tvshows.serials_users.flag_valid IS 'Connection status: activated or deactivated';
COMMENT ON COLUMN tvshows.serials_users.last_notified_episode IS 'Last sent notification';



CREATE TYPE job_status AS ENUM ('InQueue', 'InProgress', 'Success');

CREATE TABLE tvshows.jobs (
  job_id                   uuid NOT NULL UNIQUE,
  notification_info        text NOT NULL,
  endpoint_id              uuid NOT NULL,
  attempts                 int NOT NULL,
  status                   job_status NOT NULL,
  created_at               timestamp with time zone NOT NULL DEFAULT now(),
  updated_at               timestamp with time zone NOT NULL DEFAULT now(),
  worker_id                int,
  PRIMARY KEY (job_id));

COMMENT ON TABLE tvshows.jobs IS 'Queue for notification';
COMMENT ON COLUMN tvshows.jobs.job_id IS 'Unique global job id in our system';
COMMENT ON COLUMN tvshows.jobs.notification_info IS 'Text for notification';
COMMENT ON COLUMN tvshows.jobs.endpoint_id IS 'Unique global endpoint id in our system';
COMMENT ON COLUMN tvshows.jobs.attempts IS 'Count of attempts to send notification';
COMMENT ON COLUMN tvshows.jobs.status IS 'Job status';
COMMENT ON COLUMN tvshows.jobs.created_at IS 'Job creation time';
COMMENT ON COLUMN tvshows.jobs.updated_at IS 'Job last update time';
COMMENT ON COLUMN tvshows.jobs.worker_id IS 'Executor id';



GRANT ALL ON SCHEMA tvshows TO notifier;

GRANT ALL ON ALL tables IN SCHEMA tvshows TO notifier;

INSERT INTO tvshows.user_info VALUES ('1ceb8f6e-3ad6-40b8-b872-59f783d2f35e', 'John', 'Bellford');
INSERT INTO tvshows.user_info VALUES ('2f2a314a-b6e0-418f-a89f-c2d6c7e0175b', 'Will', 'Smith');
INSERT INTO tvshows.user_info VALUES ('2423444f-fc32-4922-aaf2-efa3ed3db401', 'Jadeh', 'Smith');
INSERT INTO tvshows.user_info VALUES ('78b6313e-748b-4c53-9373-da5fe082d3c8', 'Bruce', 'Willis');

INSERT INTO tvshows.serials VALUES ('d7bdbbfa-af5d-4b7a-be2a-cc9a46f7b87a', '257423', 'Rooftop Prince', '0');
INSERT INTO tvshows.serials VALUES ('0be92dee-f63a-490c-af7f-40bedbfd60a4', '348559', 'Jesus', '0');
INSERT INTO tvshows.serials VALUES ('8d387aed-ffaf-4779-897d-739d2aa216c5', '363013', 'Vet Gone Wild', '0');
INSERT INTO tvshows.serials VALUES ('cdb9d95b-ddad-4763-b155-852c144723eb', '355622', 'Doom Patrol', '0');

INSERT INTO tvshows.serials_users VALUES ('921b88ad-3354-43ba-ac8f-d8052e0ad1e2', 'd7bdbbfa-af5d-4b7a-be2a-cc9a46f7b87a',
                                          '1ceb8f6e-3ad6-40b8-b872-59f783d2f35e', 'true', 'None');

INSERT INTO tvshows.serials_users VALUES ('28a58c41-5f6b-4a89-931d-ab46db73b892', '0be92dee-f63a-490c-af7f-40bedbfd60a4',
                                          '2f2a314a-b6e0-418f-a89f-c2d6c7e0175b', 'true', 'None');

INSERT INTO tvshows.serials_users VALUES ('bcb202f8-f6a2-4d79-b8ed-800ad9b220f5', '8d387aed-ffaf-4779-897d-739d2aa216c5',
                                          '2423444f-fc32-4922-aaf2-efa3ed3db401', 'true', 'None');

INSERT INTO tvshows.serials_users VALUES ('f3d04ac9-040f-4e6f-ba8d-4d94ee5391c2', 'cdb9d95b-ddad-4763-b155-852c144723eb',
                                          '78b6313e-748b-4c53-9373-da5fe082d3c8', 'true', 'None');

INSERT INTO tvshows.user_endpoints (endpoint_id, user_id, endpoint_type, endpoint_info, flag_valid)
VALUES ('3d6b5074-9f4b-4da1-b965-6051308bdb94', '1ceb8f6e-3ad6-40b8-b872-59f783d2f35e', 'Webhook', 'https://webhook.site/b078af8e-fcbb-48b0-aa38-cad45c73bc9e', 'true'),
       ('94d60b77-6a4d-4428-9b1f-edf88a374454', '2f2a314a-b6e0-418f-a89f-c2d6c7e0175b', 'Slack', 'https://hooks.slack.com/services/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', 'true'),
       ('b7091808-1fe8-4d72-ac2d-ebbe045a7b27', '2423444f-fc32-4922-aaf2-efa3ed3db401', 'VK', '99970727', 'true'),
       ('249e8fdc-1fb4-4df5-9057-0be4fb64d400', '78b6313e-748b-4c53-9373-da5fe082d3c8', 'Email', 'ortofoxnastya@gmail.com', 'true');